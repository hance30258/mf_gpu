#include<complex>

#ifndef MF_STRUCTURE
#define MF_STRUCTURE


class straindata{

    public:

    double *data=NULL;
    int npoint;
    double srate;
    double t0;
};

class cutdataFD{

    public:
    int N;
    int npoint;
    straindata strain;
    double chunksize;
    double overlap;
    std::complex<double> *data_FD = NULL;
    double f_min;
    double f_max;
};

class waveform{

    public:
    int N;
    double srate;
    double duration;
    int get_npoint(){ return (int)srate*duration/2+1;}
    double get_deltaF(){ return 1./duration;}
    double Mmax ;
    double Mmin ;
    double f_min ;
    double f_max ;
    double *m1=NULL;
    double *m2=NULL;

    std::complex<double> *data=NULL;

};


class chisquartable{

    public:
    waveform wf;
    double *PSD=NULL;
    int nband;
    int *bandpoint=NULL;

};

#endif
