
#include<iostream>
#include<complex>
#include<cuComplex.h>
#include<cufft.h>
#include<cublas_v2.h>
#include<cuda_runtime.h>
#include"helper_cuda.h"

#include"calloc.hu"

#define NTH 256
#define Ctype cuDoubleComplex
typedef std::complex<double> Complex;

#define CHECKBLAS(call) \
{ \
    const cublasStatus_t error = call; \
    if (error != CUBLAS_STATUS_SUCCESS) \
    { \
        printf("Error: %s:%d, ", __FILE__, __LINE__); \
        printf("code:%s\n", _cudaGetErrorEnum(error)); \
        exit(1); \
    } \
}
#define CHECKFFT(call) \
{ \
    const cufftResult error = call; \
    if (error != CUFFT_SUCCESS) \
    { \
        printf("Error: %s:%d, ", __FILE__, __LINE__); \
        printf("code:%s\n", _cudaGetErrorEnum(error)); \
        exit(1); \
    } \
}
#define CHECK(call) \
{ \
    const cudaError_t error = call; \
    if (error != cudaSuccess) \
    { \
        printf("Error: %s:%d, ", __FILE__, __LINE__); \
        printf("code:%d, reason: %s\n", error, cudaGetErrorString(error)); \
        exit(1); \
    } \
}
#define EXIT(call)\
{\
    printf("Error: %s:%d, ", __FILE__, __LINE__); \
    printf("Reason: %s\n", call); \
}

using namespace std;

static __global__ void ComplexMulti( int n_chunk, int npoint, Ctype *a, Ctype *b, Ctype *out){

    const unsigned int id = threadIdx.x + blockIdx.x * blockDim.x;
    if( id >= npoint) return;
    Ctype *o_Ptr = out;
    Ctype *b_Ptr = b;
    for(int i=0; i<n_chunk; i++){
        o_Ptr[id] = cuCmul( cuConj(a[id]), b_Ptr[id]); 
        o_Ptr += npoint;
        b_Ptr += npoint;
    }
}
static __global__ void SNR_calculate( int n_chunk, int npoint, int overlap,
        double *a, double *b, double *o){

    const unsigned int id = threadIdx.x + blockIdx.x * blockDim.x;
    if( id >= npoint-overlap) return;
    int shift = overlap/2;
    double *a_Ptr = a + shift;
    double *b_Ptr = b + shift;
    double *o_Ptr = o + shift;
    for(int i=0; i<n_chunk; i++){
        double tmp = a_Ptr[id]*a_Ptr[id] + b_Ptr[id]*b_Ptr[id];
        o_Ptr[id] = sqrt(tmp);
        a_Ptr += npoint;
        b_Ptr += npoint;
        o_Ptr += npoint-overlap;
    }
}
static __global__ void rw_SNR_calculate( int npoint, int overlap, double *snr, double *chi2, double *re_snr){

    const unsigned int id = threadIdx.x + blockIdx.x * blockDim.x;
    if( id >= npoint-overlap) return;
    int shift = overlap/2;
    double chi2_tmp = chi2[id+shift];
    if( chi2_tmp < 1.0){
        re_snr[id+shift] = snr[id+shift];
    }else{
        double fact = pow((1.+chi2_tmp*chi2_tmp*chi2_tmp)/2.,-1./6.);
        re_snr[id+shift] = snr[id+shift]*fact;
    }
}
static __global__ void chi2_calculate( int n_freqbank, int npoint, int overlap,
        double *snr_o_r, double *snr_o_i, double *snr_b_r, double *snr_b_i, double *chi2){

    const unsigned int id = threadIdx.x + blockIdx.x * blockDim.x;
    if( id >= npoint-overlap) return;
    int shift = overlap/2;
    double snr_r_avg = snr_o_r[id+shift]/n_freqbank;
    double snr_i_avg = snr_o_i[id+shift]/n_freqbank;
    double *snr_b_r_Ptr = snr_b_r + shift;
    double *snr_b_i_Ptr = snr_b_i + shift;
    double chi2_tmp = 0.;
    for(int i=0; i<n_freqbank; i++){
        double r_tmp = snr_r_avg - snr_b_r_Ptr[id];
        double i_tmp = snr_i_avg - snr_b_i_Ptr[id];
        chi2_tmp += r_tmp*r_tmp + i_tmp*i_tmp;
        snr_b_r_Ptr += npoint;
        snr_b_i_Ptr += npoint;
    }
    chi2[id+shift] = chi2_tmp*n_freqbank/(2*n_freqbank-2);
}
static __global__ void SNR_max( int n_chunk, int npoint, int overlap, int tmplt_number,
        double *in_rw, double *out_rw, double *in_o, double *out_o, double *in_chi, double *out_chi, int *out_tmplt){

    const unsigned int id = threadIdx.x + blockIdx.x * blockDim.x;
    if( id >= npoint-overlap) return;
    double *in_rw_Ptr = in_rw + overlap/2;
    double *out_rw_Ptr = out_rw + overlap/2;
    double *in_o_Ptr = in_o + overlap/2;
    double *out_o_Ptr = out_o + overlap/2;
    double *in_chi_Ptr = in_chi + overlap/2;
    double *out_chi_Ptr = out_chi + overlap/2;
    int *out_tmplt_Ptr = out_tmplt + overlap/2;
    for(int i=0; i<n_chunk; i++){
        if( out_rw_Ptr[id] < in_rw_Ptr[id]){
            out_rw_Ptr[id] = in_rw_Ptr[id];
            out_o_Ptr[id] = in_o_Ptr[id];
            out_chi_Ptr[id] = in_chi_Ptr[id];
            out_tmplt_Ptr[id] = tmplt_number;
        }
        in_rw_Ptr += npoint-overlap;
        out_rw_Ptr += npoint-overlap;
        in_chi_Ptr += npoint-overlap;
        out_chi_Ptr += npoint-overlap;
        in_o_Ptr += npoint-overlap;
        out_o_Ptr += npoint-overlap;
        out_tmplt_Ptr += npoint-overlap;
    }
}
__global__ void cuprint( int size, double *a){

    int i = threadIdx.x;
    printf("a[%d] = %g\n", i, a[i]);
}

void MatchedFilterAndChisquare( int size_f,
        int n_chunk_signal, Complex *signal,
        int n_chunk, Complex *data,
        int n_freqbank, int *freqband,
        int npoint, int overlap, double **snr_out, double **re_weight_snr_out, double **chi2_out,
        int tmplt_id, int **max_tmplt_number_out){

    if( *snr_out == NULL) *snr_out = (double*)malloc_h(sizeof(double)*npoint);
    if( *re_weight_snr_out == NULL) *re_weight_snr_out = (double*)malloc_h(sizeof(double)*npoint);
    if( *chi2_out == NULL) *chi2_out = (double*)malloc_h(sizeof(double)*npoint);
    if( *max_tmplt_number_out == NULL) *max_tmplt_number_out = (int*)malloc_h(sizeof(int)*npoint);


    Complex *data_d = (Complex*)malloc_d(sizeof(Complex)*n_chunk*size_f);
  //CHECK( cudaMemcpy( data_d, data, sizeof(Complex)*n_chunk*size_f, cudaMemcpyHostToDevice));
    CHECKBLAS( cublasSetVector( size_f*n_chunk, sizeof(Complex), data, 1, data_d, 1));

    int size = size_f*2-2;
  //printf("size = %d size_f = %d overlap = %d\n", size, size_f,overlap);
    double *fft_out_r = (double*)malloc_d(sizeof(double)*n_chunk*size);
    double *fft_out_i = (double*)malloc_d(sizeof(double)*n_chunk*size);
    Ctype *fft_in = (Ctype*)malloc_d(sizeof(Ctype)*n_chunk*size_f); 

    cufftHandle plan, plan_chi;
    CHECKFFT( cufftPlan1d( &plan, size, CUFFT_Z2D, n_chunk));
    CHECKFFT( cufftPlan1d( &plan_chi, size, CUFFT_Z2D, n_freqbank));
    Ctype *fft_in_chi = (Ctype*)malloc_d(sizeof(Ctype)*n_freqbank*size_f);
    CHECK( cudaMemset( fft_in_chi, 0, sizeof(Ctype)*n_freqbank*size_f));
    double *fft_out_r_chi = (double*)malloc_d(sizeof(double)*n_freqbank*size);
    double *fft_out_i_chi = (double*)malloc_d(sizeof(double)*n_freqbank*size);

    cublasHandle_t handle;
    CHECKBLAS( cublasCreate( &handle));

    double *snr = (double*)malloc_d(sizeof(double)*npoint);
    double *re_weight_snr = (double*)malloc_d(sizeof(double)*npoint);
    double *chi2 = (double*)malloc_d(sizeof(double)*npoint);

    double *snr_max = (double*)malloc_d(sizeof(double)*npoint);
    CHECK( cudaMemcpy( snr_max, *snr_out, sizeof(double)*npoint, cudaMemcpyHostToDevice)); 
    double *chi2_max = (double*)malloc_d(sizeof(double)*npoint);
    CHECK( cudaMemcpy( chi2_max, *chi2_out, sizeof(double)*npoint, cudaMemcpyHostToDevice)); 
    double *re_weight_snr_max = (double*)malloc_d(sizeof(double)*npoint);
    CHECK( cudaMemcpy( re_weight_snr_max, *re_weight_snr_out, sizeof(double)*npoint, cudaMemcpyHostToDevice)); 
    int *max_tmplt_number_max = (int*)malloc_d(sizeof(int)*npoint);
    CHECK( cudaMemcpy( max_tmplt_number_max, *max_tmplt_number_out, sizeof(int)*npoint, cudaMemcpyHostToDevice));

    Complex *signal_d = (Complex*)malloc_d(sizeof(Complex)*size_f);
    Complex *signal_Ptr = signal;
    int *freqband_Ptr = freqband;
    int *Idx_max = (int*)malloc_u(sizeof(int));
    for(int i=0; i<n_chunk_signal; i++){

        CHECKBLAS( cublasSetVector( size_f, sizeof(Complex), signal_Ptr, 1, signal_d, 1));
      //CHECK( cudaMemcpy( signal_d, signal_Ptr, sizeof(Ctype)*size_f, cudaMemcpyHostToDevice));

        int block = (size_f+NTH-1)/NTH;
        ComplexMulti <<< block, NTH>>>( n_chunk, size_f, (Ctype*)signal_d, (Ctype*)data_d, fft_in);

        CHECKFFT( cufftExecZ2D( plan, fft_in, fft_out_r));

        Ctype I = make_cuDoubleComplex( 0.0, -1.0);
        CHECKBLAS( cublasZscal( handle, n_chunk*size_f, &I, (Ctype*)fft_in, 1));

        CHECKFFT( cufftExecZ2D( plan, fft_in, fft_out_i));

        int block_c = (size-overlap+NTH-1)/NTH;
        SNR_calculate <<< block_c, NTH>>>
            ( n_chunk, size, overlap, fft_out_r, fft_out_i, snr);

        double *snr_Ptr = snr;
        double *chi2_Ptr = chi2;
        double *re_weight_snr_Ptr = re_weight_snr;
        double *fft_out_r_Ptr = fft_out_r;
        double *fft_out_i_Ptr = fft_out_i;
        Ctype *fft_in_Ptr = fft_in;
        for(int k=0; k<n_chunk; k++){
            double max_value;
            CHECKBLAS( cublasIdamax( handle, size-overlap, snr_Ptr, 1, Idx_max));
            CHECKBLAS( cublasGetVector( 1, sizeof(double), &snr_Ptr[*Idx_max], 1, &max_value, 1));
            if( max_value >= 6.0 || true) {
                if( max_value >= 6.0) printf("SNR > 6 at %d chunk %lf\n", k, max_value);
                CHECK( cudaMemset( fft_in_chi, 0, sizeof(Ctype)*n_freqbank*size_f));
                Ctype *fft_in_chi_Ptr = fft_in_chi;
                for(int j=0;j<n_freqbank;j++) {
                    CHECK( cudaMemcpy( &fft_in_chi_Ptr[freqband_Ptr[j]], &fft_in_Ptr[freqband_Ptr[j]], 
                            sizeof(Ctype)*(freqband_Ptr[j+1]-freqband_Ptr[j]), cudaMemcpyDeviceToDevice));
                    fft_in_chi_Ptr += size_f;
                }
                CHECKFFT( cufftExecZ2D( plan_chi, fft_in_chi, fft_out_i_chi));

                fft_in_chi_Ptr = fft_in_chi;
                for(int j=0;j<n_freqbank;j++) {
                    Ctype minus_I = make_cuDoubleComplex( 0.0, 1.0);
                    CHECKBLAS( cublasZscal( handle, freqband_Ptr[j+1]-freqband_Ptr[j],
                                &minus_I, &fft_in_chi_Ptr[freqband_Ptr[j]], 1));
                    fft_in_chi_Ptr += size_f;
                }
                CHECKFFT( cufftExecZ2D( plan_chi, fft_in_chi, fft_out_r_chi));
                chi2_calculate <<< block_c, NTH>>>( n_freqbank, size, overlap,
                        fft_out_r_Ptr, fft_out_i_Ptr, fft_out_r_chi, fft_out_i_chi, chi2_Ptr);
                rw_SNR_calculate <<< block_c, NTH>>>( size, overlap, snr_Ptr, chi2_Ptr, re_weight_snr_Ptr);
            }


            snr_Ptr += size-overlap;
            chi2_Ptr += size-overlap;
            re_weight_snr_Ptr += size-overlap;
            fft_in_Ptr += size_f;
            fft_out_r_Ptr += size;
            fft_out_i_Ptr += size;
        }

        SNR_max <<< block_c, NTH>>> ( n_chunk, size, overlap, tmplt_id+i,
                snr, snr_max, chi2, chi2_max, re_weight_snr, re_weight_snr_max, max_tmplt_number_max);

        signal_Ptr += size_f;
        freqband_Ptr += n_freqbank+1;
    }


    CHECK( cudaMemcpy( *snr_out, snr_max, sizeof(double)*npoint, cudaMemcpyDeviceToHost));
    CHECK( cudaMemcpy( *chi2_out, chi2_max, sizeof(double)*npoint, cudaMemcpyDeviceToHost));
    CHECK( cudaMemcpy( *re_weight_snr_out, re_weight_snr_max, sizeof(double)*npoint, cudaMemcpyDeviceToHost));
    CHECK( cudaMemcpy( *max_tmplt_number_out, max_tmplt_number_max, sizeof(int)*npoint, cudaMemcpyDeviceToHost));


  //double *a = (double*)malloc_u(sizeof(double)*size);
  //CHECK( cudaMemset( a, 0, sizeof(double)*size)); 
  //cuprint <<< 1, NTH>>>( size, a);
  //cudaDeviceSynchronize();
  //for(int i=0; i<NTH; i++) printf("cpu a[%d] = %g\n", i, a[i]);
  //CHECK( cudaFree( a));



    CHECK( cudaFree( data_d));
    CHECK( cudaFree( signal_d));

    CHECK( cudaFree( fft_out_r));
    CHECK( cudaFree( fft_out_i));
    CHECK( cudaFree( fft_out_r_chi));
    CHECK( cudaFree( fft_out_i_chi));
    CHECK( cudaFree( fft_in));
    CHECK( cudaFree( fft_in_chi));

    CHECK( cudaFree( Idx_max));

    CHECKFFT( cufftDestroy( plan));
    CHECKFFT( cufftDestroy( plan_chi));
    CHECKBLAS( cublasDestroy( handle));
    CHECK( cudaFree( snr));
    CHECK( cudaFree( re_weight_snr));
    CHECK( cudaFree( chi2));
    CHECK( cudaFree( snr_max));
    CHECK( cudaFree( re_weight_snr_max));
    CHECK( cudaFree( chi2_max));
    CHECK( cudaFree( max_tmplt_number_max));
}
