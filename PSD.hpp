/* This file was automatically generated.  Do not edit! */
double *linearinterpolation(int no,int ni,double *iSn);
double *psd(straindata idata,double chunk,double *wfunc);
double *windowfunc(int n);
double *tukeywindowfunc(int n,double a);
straindata data_whiten(straindata idata,double *PSD);
void data_FD_whiten(cutdataFD *idata,double *PSD);
void read_data(char *filename,char *channel,straindata *idata);
