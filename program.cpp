#include<stdio.h>
#include<iostream>
#include<fstream>
#include<complex>
#include<cuda_runtime.h>
#include<omp.h>
#include"cxxopts.hpp"
extern"C"{
#include<lal/LALSimInspiral.h>
#include<lal/LALConstants.h>
#include<lal/FrequencySeries.h>
}

#include"calloc.hu"
#include"MFstruct.cpp"
#include"lalcode.hpp"
#include"PSD.hpp"
#include"chisquare.hu"
#include"X2.hu"
#include"mf_kernal.hu"

using namespace std;
typedef std::complex<double> Complex;

double *read_bank( char *filename, int *npoint){

    printf("read bankfile  :%s\n", filename);

    int n = 0;
    double c,d;
    FILE *fr_r = fopen(filename,"r");
    while( fscanf(fr_r,"%lf %lf\n",&c,&d)!=EOF){
        n++;
    }
    fclose(fr_r);

    printf("read line    : %d\n", n);
    double *out = (double*)malloc(sizeof(double)*n*2);

    FILE *fp_r = fopen(filename,"r");
    for(int i=0;i<n;i++){
        fscanf(fp_r,"%lf %lf\n",&c,&d);
        if( c==0 || d==0)printf("ERROR read bank %d\n",i);
        out[i] = c;
        out[i+n] = d;
    }
    fclose(fp_r);
    *npoint = n;
    return out;
}
Approximant choose_approximant( waveform Tp){
    double max_mchirp = 0;
    for(int i=0; i<Tp.N; i++){
      double mchirp = pow((Tp.m1[i] * Tp.m2[i]), 3.0/5.0) / pow((Tp.m1[i]+Tp.m2[i]), 1.0/5.0);
      if( max_mchirp < mchirp) max_mchirp = mchirp;
    }
    if( max_mchirp < 2.0){
        return TaylorF2;
    }else{
        return SEOBNRv4_ROM;
    }
}


int main( int argc, char **argv){

    cxxopts::Options options("matched filter pipeline", " input gwf file & tempbank");

    bool verbose= false;
    bool help   = false;
    options.add_options()
        ("i,infile", "input gwf file", cxxopts::value<std::string>())
        ("c,channel", "gwf channel", cxxopts::value<std::string>())
        ("o,outfile", "snr result", cxxopts::value<std::string>())
        ("v,verbose", "verbose output", cxxopts::value(verbose))
        ("chunksize", "set MF chunksize",cxxopts::value<double>())
        ("b,bankfile", "templatebank file",cxxopts::value<std::string>())
        ("approximant", "approximant",cxxopts::value<std::string>())
        ("specturmfile", "output specturm for each chunk & return",cxxopts::value<std::string>())
        ("flow", "freq highpass",cxxopts::value<double>())
        ("m1min", "min m1",cxxopts::value<double>())
        ("m1max", "max m1",cxxopts::value<double>())
        ("m2min", "min m2",cxxopts::value<double>())
        ("m2max", "max m2",cxxopts::value<double>())
        ("n,n_tmplt", "n_tmplt",cxxopts::value<int>())
        ("h,help", "help",cxxopts::value(help))
        ;

    auto result = options.parse(argc,argv);
    if( help) cout << options.help() << endl;
    if( help) return 0;
    if( verbose) cout << "verbose on" << endl;

    straindata idata;
    if( not (result.count("infile") && result.count("channel"))){
        cout << "Error : no input strain file use -h for more info" << endl;
        return 0;
    }

    string ifile = result["infile"].as<std::string>();
    string channel = result["channel"].as<std::string>();
    if( verbose) cout << "read file : " << ifile << " channel : " << channel << endl;

    read_data( (char*)ifile.c_str(), (char*)channel.c_str(), &idata); 
  //idata.data += (int)idata.srate*(2048-128);
  //idata.t0 += 2048-128;
  //idata.npoint = 256*idata.srate;

    if( verbose) cout << "sampleRate|" << idata.srate << " tRange|" << idata.npoint/idata.srate << endl;
    cutdataFD datacut;
    datacut.strain = idata;

    double chunksize = 512;
    if( result.count("chunksize")) chunksize = result["chunksize"].as<double>();
    if( result.count("chunksize") && result.count("specturmfile")){


    }

    waveform Tp;
    Tp.N = 1;
    Tp.srate = idata.srate;
    if( result.count("n_tmplt")) Tp.N = result["n_tmplt"].as<int>();
    Tp.Mmax =  36;
    Tp.Mmin =  29;
    Tp.f_min = 20;
    if( result.count("flow")) Tp.f_min = result["flow"].as<double>();
    Tp.f_max = 2048;

    double *tmpltbank = NULL;
    int n_tmpltbank = 0;
    if( result.count("bankfile")){
        string bankfile = result["bankfile"].as<std::string>();
        tmpltbank = read_bank( (char*)bankfile.c_str(), &n_tmpltbank);
        Tp.m1 = tmpltbank;
        Tp.m2 = tmpltbank+n_tmpltbank;
        if( result.count("m1min") &&result.count("m1max") &&result.count("m2min") &&result.count("m1max")){

            double m1min = result["m1min"].as<double>();
            double m2min = result["m2min"].as<double>();
            double m1max = result["m1max"].as<double>();
            double m2max = result["m2max"].as<double>();
            int count = 0;
            for(int i=0;i<n_tmpltbank;i++){
                if( tmpltbank[i] < m1max && tmpltbank[i] > m1min &&
                        tmpltbank[i+n_tmpltbank] < m2max && tmpltbank[i+n_tmpltbank] > m2min){
                    tmpltbank[count] = tmpltbank[i];
                    tmpltbank[count+n_tmpltbank] = tmpltbank[i+n_tmpltbank];
                    count++;
                    printf("%lf %lf\n", tmpltbank[count], tmpltbank[count+n_tmpltbank]);
                }
            }
            n_tmpltbank = count;
            if( verbose) printf("template count = %d\n", n_tmpltbank);
        }

    }else{
        n_tmpltbank = Tp.N;
    }

    int NGPU;
    cudaGetDeviceCount( &NGPU);// NGPU = 1;
    printf("cudaGetDeviceCount : %d\n", NGPU);

    waveform wf[NGPU];
    
    double *wdfunction = NULL;
    double *PSD = NULL;
    double *snr[NGPU];
    double *chi2[NGPU];
    double *rw_snr[NGPU];
    int *max_tmplt_number[NGPU];
    for(int i=0;i<NGPU;i++){
        snr[i] = NULL;
        chi2[i] = NULL;
        rw_snr[i] = NULL;
        max_tmplt_number[i] = NULL;
    }
    for(int i=0;i<n_tmpltbank;i+=Tp.N){

        if( i+Tp.N > n_tmpltbank) Tp.N = n_tmpltbank-i;

        if( verbose) printf("complete %d/%d\n",i,n_tmpltbank);

        double chunksize;
        waveform_init_rd( &Tp, &chunksize);
        Tp.m1[0] = 40;
        Tp.m2[0] = 32;
        Approximant appr = choose_approximant(Tp);
        if( verbose) cout << "Approximant : " << appr << endl;

        if( chunksize != datacut.chunksize){
            free(PSD);
            free(wdfunction);

            double psdsize = 1.;
            wdfunction = windowfunc( idata.srate*psdsize);
            //PSD = psd( idata, psdsize, wdfunction);
            PSD = linearinterpolation( chunksize*idata.srate/2+1, psdsize*idata.srate/2+1, psd( idata, psdsize, wdfunction));
            datacut.chunksize = chunksize;
            datacut.overlap = chunksize/2;

            data_FD_whiten( &datacut, PSD);
            
          //FILE *psd_ch = fopen("spectrum_chunk","w");
          //printf("checkpoint\n");
          //if( datacut.data_FD==NULL) exit(1);
          //for(int j=0;j<datacut.N*datacut.npoint;j+=4){
          //    fprintf( psd_ch,"%15.12lf %e\n",(double)(j%datacut.npoint)*Tp.get_deltaF(),abs(datacut.data_FD[j]));
          //}
          //fclose( psd_ch);
            if( verbose) printf("N=%d chunk=%g\n", datacut.N, datacut.chunksize);
        }
        omp_get_max_threads();
        waveform_generate( Tp, appr);

        int subN = Tp.N/NGPU; 
        int lesN = Tp.N-NGPU*subN;
        int count = 0;
        for(int igpu=0;igpu<NGPU;igpu++){
            wf[igpu] = Tp;
            wf[igpu].m1 += igpu*subN;
            wf[igpu].m2 += igpu*subN;
            wf[igpu].N = subN;
            if( igpu<lesN) wf[igpu].N++;
            count += wf[igpu].N;
            wf[igpu].data += Tp.get_npoint()*(count-wf[igpu].N);
        }

        omp_set_num_threads(NGPU);
        int id_cpu;
#pragma omp parallel private( id_cpu)
        {
            id_cpu = omp_get_thread_num();
            cudaSetDevice( id_cpu);

          //omp_set_num_threads(10);
            waveform_normalize( wf[id_cpu], PSD);


            chisquartable table;
            table.wf = wf[id_cpu];
            table.PSD = PSD;
            table.nband = 8;

            chisquare_init( &table);

            MatchedFilterAndChisquare( wf[id_cpu].get_npoint(),
                    wf[id_cpu].N, wf[id_cpu].data,
                    datacut.N, datacut.data_FD,
                    table.nband, table.bandpoint,
                    idata.npoint, datacut.overlap*idata.srate, &snr[id_cpu], &rw_snr[id_cpu], &chi2[id_cpu],
                    i, &max_tmplt_number[id_cpu]);
            free(table.bandpoint);
        }


        Tp.m1 +=Tp.N;
        Tp.m2 +=Tp.N;
    }
    printf("END OF CODE\n");

    for(int j=0;j<idata.npoint;j++){
    for(int i=1;i<NGPU;i++){
        if( snr[0][j] < snr[i][j]){
            snr[0][j] = snr[i][j];
            rw_snr[0][j] = rw_snr[i][j];
            chi2[0][j] = chi2[i][j];
            max_tmplt_number[0][j] = max_tmplt_number[i][j];
        }
    }
    }

    if( result.count("outfile")){
        string outfile = result["outfile"].as<std::string>();

        FILE *out = fopen( (char*)outfile.c_str(),"w");
        for(int i=0; i<idata.npoint;i++)
            fprintf(out,"%lf %lf %lf %lf %d\n", 1./idata.srate*i, rw_snr[0][i], snr[0][i], chi2[0][i], max_tmplt_number[0][i]);
        fclose(out);
        if( verbose)
            cout << "outputfile : `" << outfile << "` format : |time|re-w|snr|chi2|"<< endl;
    }
    return 0;
}
