
#include<stdio.h>
#include<stdlib.h>

#include<cuda_runtime.h>
#include<omp.h>
#include<complex>
extern"C"{
#include<lal/LALSimInspiral.h>
#include<lal/LALConstants.h>
#include<lal/FrequencySeries.h>
}

#include"calloc.hu"
#include"MFstruct.cpp"


void lal_fd_waveform( double m1, double m2, double deltaF, Approximant approximant, COMPLEX16FrequencySeries **hptilde , COMPLEX16FrequencySeries **hctilde ){
  m1 *=  LAL_MSUN_SI;     // mass of body 1
  m2 *=  LAL_MSUN_SI;     // mass of body 2
  double S1x = 0.0;                  // x-component of dimensionless spin of body 1
  double S1y = 0.0;                  // y-component of dimensionless spin of body 1
  double S1z = 0.0;                  // z-component of dimensionless spin of body 1
  double S2x = 0.0;                  // x-component of dimensionless spin of body 2
  double S2y = 0.0;                  // y-component of dimensionless spin of body 2
  double S2z = 0.0;                  // z-component of dimensionless spin of body 2
  double distance = 1e6 * LAL_PC_SI; // distance
  double inclination = 0.0;          // inclination
  double phiRef = 0;                 // gravitational wave phase at end
  double longAscNodes=0.0;           // longitude of ascending nodes, degenerate with the polarization angle, Omega in documentation
  double eccentricity=0.0;           // eccentricity at reference epoch
  double meanPerAno=0.0;             // mean anomaly of periastron
  //double deltaF = 1./32;                // frequency sampling interval
//double f_min = cutofftime( 1./deltaF, m1, m2);
  double f_min = 20;
//printf("f_min %g m1 %g m2 %g\n", f_min,m1,m2);
  double f_max = 0.0;                // end frequency of inspiral: 0 means use default
  double f_ref = 0.0;                // reference frequency: 0 means waveform end
  LALDict *LALpars = NULL;       // structure containing variable with default values

  XLALSimInspiralChooseFDWaveform( hptilde, hctilde, m1, m2, S1x, S1y, S1z, S2x, S2y, S2z, distance, inclination, phiRef, longAscNodes, eccentricity, meanPerAno, deltaF, f_min, f_max, f_ref, LALpars, approximant);

}

double waveform_duration( double m1, double m2, double f_cut){

    double duration;
    double ninta = m1*m2/(m1+m2)/(m1+m2);
    double Mc = (m1+m2)*pow( ninta, 3./5);

    duration = 647013/ pow( f_cut, 8./3)/pow( Mc, 5./3);
    return duration;
  //printf("m1=%g-m2=%g-duration=%g\n", m1, m2, duration);
}


void waveform_init_rd( waveform *wfdata, double *origin_duration){

    if( wfdata->N==0) {
        printf("init Nwaveform = %d\n", 400);
        exit(-1);
    }
    if( wfdata->get_npoint()==0){
        printf("Plz set npoint of waveform\n");
        exit(-1);
    }

    if( wfdata->m1==NULL && wfdata->m2==NULL){
        wfdata->m1 = (double*)malloc(sizeof(double)* wfdata->N);
        wfdata->m2 = (double*)malloc(sizeof(double)* wfdata->N);
        for( int i=0;i<wfdata->N;i++){
            wfdata->m1[i] = (float)rand()/(float)(RAND_MAX/(wfdata->Mmax-wfdata->Mmin))+wfdata->Mmin;
            wfdata->m2[i] = (float)rand()/(float)(RAND_MAX/(wfdata->Mmax-wfdata->Mmin))+wfdata->Mmin;
        }
    }

    double max_duration = 0;
    for( int i=0;i<wfdata->N;i++){

      double duration = waveform_duration( wfdata->m1[i], wfdata->m2[i], wfdata->f_min);
      if( max_duration < duration) max_duration = duration;
    }
    double chunksize = 2;
    while( chunksize < max_duration*4+6) chunksize *=2;
    if( chunksize != *origin_duration) {
        if( wfdata->data != NULL) cudaFreeHost( wfdata->data);
        *origin_duration = chunksize;
        wfdata->duration = chunksize;
        wfdata->data = (std::complex<double>*)malloc_h(sizeof(std::complex<double>)*wfdata->get_npoint()*wfdata->N);
    }
}
void waveform_generate( waveform wfdata, Approximant approximant){

#pragma omp parallel for
    for( int i=0; i<wfdata.N;i++){
      //int k = omp_get_thread_num();
      //printf("i = %d ithread = %d\n", i, k);
        COMPLEX16FrequencySeries *hplus = NULL;
        COMPLEX16FrequencySeries *hcross = NULL;
        lal_fd_waveform( wfdata.m1[i], wfdata.m2[i], wfdata.get_deltaF(), approximant, &hplus, &hcross);
        if(!hplus)printf("error\n");

        waveform_duration( wfdata.m1[i], wfdata.m2[i], wfdata.f_min);
        std::complex<double> *tmp = &wfdata.data[ i*wfdata.get_npoint()];
        for( int j=wfdata.f_min/wfdata.get_deltaF() ;j<fmin(hplus->data->length,wfdata.f_max/wfdata.get_deltaF());j++){
            double a = creal( hplus->data->data[j]);
            double b = cimag( hplus->data->data[j]);
            tmp[j] = { a, b};
        }
        for( int j=hplus->data->length;j<wfdata.f_max/wfdata.get_deltaF();j++) tmp[j] = { 0., 0.};
        for( int j=wfdata.f_max/wfdata.get_deltaF(); j< wfdata.get_npoint(); j++) tmp[j] = { 0., 0.};
        for( int j= 0; j<wfdata.f_min/wfdata.get_deltaF(); j++) tmp[j] = { 0., 0.};

        XLALDestroyCOMPLEX16FrequencySeries( hplus);
        XLALDestroyCOMPLEX16FrequencySeries( hcross);
    }

  //printf("\nwaveform end\n");
}

void waveform_normalize( waveform wfdata, double *PSD){

  for( int i=0; i<wfdata.N; i++){
      std::complex<double> *tmp = wfdata.data + i*wfdata.get_npoint();

    double Sum = 0;
    for(int j=wfdata.f_min/wfdata.get_deltaF(); j<wfdata.f_max/wfdata.get_deltaF(); j++){
      Sum += norm(tmp[j])/PSD[j] ;
    }
    Sum = 2.0*sqrt( Sum*wfdata.get_deltaF());

  //printf("m1 m2 = %e %e\n", wfdata.m1[i], wfdata.m2[i]);
    if( Sum==0.0)printf("waveform generate ERROR\n");

    for(int j=wfdata.f_min/wfdata.get_deltaF(); j<wfdata.f_max/wfdata.get_deltaF(); j++){
      tmp[j] /= Sum;
    }
  }

}
