
#include<stdio.h>
#include<stdlib.h>
#include<iostream>
#include<complex>
#include<math.h>
#include<complex.h>
#include"FrameL.h"
#include<fftw3.h>

#include"calloc.hu"

#include"MFstruct.cpp"

typedef std::complex<double> Complex;

void read_data( char *filename, char *channel, straindata *idata){

    if( filename==NULL) printf("ERROR: NO inp gwffile\n");
    FrFile *hf = FrFileINew( filename);
    FrameH *frame = FrameRead( hf);
  //printf("read file : %s\n",filename);

    FrProcData *vect = FrProcDataFind( frame, channel);

    idata->npoint = vect->data->nData;
    idata->srate = vect->data->nData/(int)vect->tRange;
    //idata->srate = 4096;
    idata->t0 = vect->data->GTime;

    double *data = (double*)malloc( sizeof(double)*vect->data->nData);
    for(int i=0;i<vect->data->nData;i++) data[i] = vect->data->dataD[i];
    idata->data = data;
  //printf("end of read data\n");
    FrameFree(frame);
    FrFileIEnd(hf);
}
straindata data_whiten( straindata idata, double *PSD){

    straindata whiten_data = idata;

    fftw_plan p;
    double *input = whiten_data.data;
    fftw_complex *data_fd = (fftw_complex*)malloc( sizeof(fftw_complex)*(whiten_data.npoint/2+1));
    p = fftw_plan_dft_r2c_1d( whiten_data.npoint, input, data_fd, FFTW_ESTIMATE);
    fftw_execute(p);

    Complex *tmp = (Complex*)data_fd;
    for( int j=0; j<whiten_data.npoint/2+1; j++){
        tmp[j] /= sqrt(PSD[j])*idata.npoint;
    }
    for( int j=0; j<whiten_data.npoint/whiten_data.srate*20; j++){
        tmp[j] = { 0., 0.};
    }

    double *odata = (double*)malloc( sizeof(double)*(whiten_data.npoint));

    fftw_plan pr;
    pr = fftw_plan_dft_c2r_1d( whiten_data.npoint, data_fd, odata, FFTW_ESTIMATE);
    fftw_execute(pr);

    whiten_data.data = odata;

    free( data_fd);

    return whiten_data;
}

double *tukeywindowfunc( int n, double a){

    double *wfunc = (double*)malloc(sizeof(double)*n);
    for(int i=0;i<n;++i){
        wfunc[i] = 1;
        if( i < a*(n-1)/2) wfunc[i] = .5*( 1.+cos( M_PI*( 2.*i/a/(n-1) -1)));
        if( i < n && i > (n-1)*(1.-a/2)) wfunc[i] = .5*( 1.+cos( M_PI*( 2.*i/a/(n-1) -2./a -1)));
    }
    return wfunc;
}

double *windowfunc( int n){

    double norm = 0;
    double w = 2.*M_PI/(n-1);
    double *wfunc = (double*)malloc(sizeof(double)*n);
    for(int i=0;i<n;++i){
        wfunc[i] = .5 - .5*cos( w*i);
      //wfunc[i] = 25./46 - 21./46*cos( w*i);
      //wfunc[i] = 0.355768 - 0.487396*cos(w*i) + 0.144232*cos(2.*w*i) - 0.012604*cos(3.*w*i);
        norm += wfunc[i]*wfunc[i];
    }
    norm = sqrt( norm*n/2);
    for(size_t i=0;i<n;++i) wfunc[i] /= norm;
    return wfunc;
}
double *psd( straindata idata, double chunk, double *wfunc){
    
    size_t i, j;
    int overlap = 2;
    int nc = chunk*idata.srate;
    int n = idata.npoint;
    double *tdata = (double*)malloc(sizeof(double)*nc);
    double *sdata = (double*)malloc(sizeof(double)*(nc/2+1));
    fftw_complex *fdata = (fftw_complex*)malloc(sizeof(fftw_complex)*(nc/2+1));

    fftw_plan p;
    p = fftw_plan_dft_r2c_1d( nc, tdata, fdata, FFTW_ESTIMATE);
    for( i=0;i<nc/2+1;++i) sdata[i] = 0.;
    for( j=0;j<n-nc+1;j+=nc/overlap){
        double shap = idata.data[j+nc]-idata.data[j];
      //for( i=0;i<nc;++i) tdata[i] = idata->data[i+j] - idata->data[j] - shap*i/nc;
      //for( i=0;i<nc;++i) tdata[i] *= wfunc[i];
        for( i=0;i<nc;++i) tdata[i] = (idata.data[i+j]-shap)*wfunc[i];
        fftw_execute(p);
        for( i=0;i<nc/2+1;++i) sdata[i] += (fdata[i][0]*fdata[i][0]+fdata[i][1]*fdata[i][1]);
      //for( i=0;i<nc/2+1;++i) sdata[i] += 1./(fdata[i][0]*fdata[i][0]+fdata[i][1]*fdata[i][1]);
    }
    double *out = (double*)malloc(sizeof(double)*(nc/2+1));
    for( i=0;i<nc/2+1;++i) out[i] = chunk/(n/nc*overlap-overlap+1)*sdata[i];
  //for( i=0;i<nc/2+1;++i) out[i] = chunk*(n/nc*overlap-overlap+1)/sdata[i]*8;
    for( i=0;i<nc/2+1;++i) if( out[i]==0)printf("ERROR : psd=0!");

    fftw_destroy_plan(p);
    free( tdata);
    free( sdata);
    free( fdata);
    return out;
}

void data_FD_whiten( cutdataFD *idata, double *PSD){

    int chunk = idata->chunksize*idata->strain.srate;
    int srate = idata->strain.srate;
    int n = (idata->strain.npoint-srate*idata->overlap)/(idata->chunksize-idata->overlap)/srate;
  //int n = 1;
    idata->N = n;
    idata->npoint = chunk/2+1;
    if( idata->data_FD != NULL) cudaFreeHost( idata->data_FD);
    idata->data_FD = (Complex*)malloc_h( sizeof(Complex)*(chunk/2+1)*n);

    double *data = (double*)malloc(sizeof(double)*chunk);
    double *tukey = tukeywindowfunc( chunk, 0.1);
  //FILE *tukfile = fopen("tukeywindow.txt","w");
  //for( int i=0; i<chunk; i++) fprintf( tukfile,"%lf %e\n", (double)i/srate, tukey[i]);
  //fclose( tukfile);
  //printf("outfile tukey!\n");

    fftw_plan p;
    for( int i=0; i<n; i++){
        double *input = &idata->strain.data[i*(int)(srate*(idata->chunksize-idata->overlap))];
        fftw_complex *output = (fftw_complex*)(idata->data_FD + (chunk/2+1)*i);
        for( int j=0; j<chunk; j++) data[j] = input[j]*tukey[j];
      //for( int j=0; j<chunk; j++) data[j] = input[j];
        p = fftw_plan_dft_r2c_1d( chunk, data, output, FFTW_ESTIMATE);
        fftw_execute(p);
    }
    for( int i=0; i<n ;i++){
        Complex *tmp = &idata->data_FD[i*(chunk/2+1)];
        for( int j=0; j<chunk/2+1; j++){
            if( tmp[j].real()==0.0)printf("Er:straindata!%g\n",tmp[j].imag());
            tmp[j] /= ( PSD[j]*chunk/2);
            if( tmp[j].real()==0.0)printf("Er:PSD!%d\n",j);
        }
    }
    free(data);
    free(tukey);
}

double *linearinterpolation( int no, int ni, double *iSn){

    double *oSn = (double*)malloc(sizeof(double)*no);
    printf("%d %d\n", no, ni);
    int ratio = (no-1)/(ni-1);
    if( ratio<1) printf("Error Linearinterpolation %g\n", ratio);
    if( ratio == 1) {
        printf("Linearinterpolation: same sample rate\n");
        return iSn;
    }
    for( int i=0;i<no;i++){
        oSn[i] = (iSn[i/ratio+1]*(i%ratio)+iSn[i/ratio]*(ratio-i%ratio))/ratio;
      //printf("%d %e = %e\n",i, oSn[i], iSn[i/ratio]);
    }
    free( iSn);
    return oSn;
}
