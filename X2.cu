
#include<stdio.h>
#include<iostream>
#include<cmath>
#include<complex>
#include<cuComplex.h>
#include<cufft.h>
#include<cuda_runtime.h>

#include"calloc.hu"
#include"MFstruct.cpp"

#define nth 256

#define CHECKFFT(call) \
{ \
    const cufftResult error = call; \
    if (error != CUFFT_SUCCESS) \
    { \
        printf("Error: %s:%d, ", __FILE__, __LINE__); \
        printf("code:%d\n", error); \
        exit(1); \
    } \
}
#define CHECK(call) \
{ \
const cudaError_t error = call; \
if (error != cudaSuccess) \
{ \
printf("Error: %s:%d, ", __FILE__, __LINE__); \
printf("code:%d, reason: %s\n", error, cudaGetErrorString(error)); \
exit(1); \
} \
}
#define EXIT(call)\
{\
printf("Error: %s:%d, ", __FILE__, __LINE__); \
printf("Reason: %s\n", call); \
}
__global__ static void chi2_equ( int npoint, int overlap, double *chi2, double *snr){

    unsigned int id = threadIdx.x + blockIdx.x * blockDim.x ;
    unsigned int k = id + overlap/2;

    if( id >= npoint-overlap) return;
    if( snr[k] < chi2[k]) snr[k] = chi2[k];
}
__global__ static void re_weight_rho( int npoint, int overlap, double *chi2, double *snr){

    unsigned int id = threadIdx.x + blockIdx.x * blockDim.x ;
    unsigned int k = id + overlap/2;

    if( id >= npoint-overlap) return;
    if( chi2[k] <= 1) return;
    double fact = 1./pow((1+pow(chi2[k],3))/2,1./6);
    snr[k] *= fact;
  //snr[k] = chi2[k];
}

__global__ static void X2_calculate( int npoint, int overlap, int nband, double *in_r, double *in_i, double *chi_r, double *chi_i, double *out){

    unsigned int id = threadIdx.x + blockIdx.x * blockDim.x ;
    if( id >= npoint-overlap) return;

    unsigned int k = id + overlap/2;

    double average_rho_r = 0;
    double average_rho_i = 0;
    double sum = 0;
  //for(int i=0; i<nband; i++){
  //    average_rho_r += chi_r[k+i*npoint]/nband;
  //    average_rho_i += chi_i[k+i*npoint]/nband;
  //}
    average_rho_r = in_r[k]/nband;
    average_rho_i = in_i[k]/nband;
    for(int i=0; i<nband; i++){
        double real = chi_r[k+i*npoint] - average_rho_r;
        double imag = chi_i[k+i*npoint] - average_rho_i;
      //sum += chi_r[k+i*npoint]+chi_i[k+i*npoint];
      //sum += real + imag;
        sum += real*real + imag*imag;
    }
    out[k] = nband*sum/(2*nband-2);
}
__global__ void rho_calculate( int npoint, cuDoubleComplex *u, cuDoubleComplex *h, cuDoubleComplex *out){

    unsigned int k = threadIdx.x + blockIdx.x * blockDim.x ;
    unsigned int m = threadIdx.y + blockIdx.y * blockDim.y ;
    unsigned int n = threadIdx.z + blockIdx.z * blockDim.z ;
    if(  k < npoint ){
        unsigned int id = k+m*npoint+n*blockDim.y*gridDim.y*npoint;
        out[id] = cuCmul( u[ k+m*npoint] , cuConj( h[ k+n*npoint])) ;
    }
}
__global__ void rho_phasei( int npoint, cuDoubleComplex *out){

    unsigned int k = threadIdx.x + blockIdx.x * blockDim.x ;
    unsigned int m = threadIdx.y + blockIdx.y * blockDim.y ;
    unsigned int n = threadIdx.z + blockIdx.z * blockDim.z ;
    if( k < npoint ){
        unsigned int id = k+m*npoint+n*blockDim.y*gridDim.y*npoint;
        out[id] = cuCmul( out[id], make_cuDoubleComplex( 0.0, 1.0));
    }
}
__global__ void rho_phaser( int npoint, cuDoubleComplex *out){

    unsigned int k = threadIdx.x + blockIdx.x * blockDim.x ;
    unsigned int m = threadIdx.y + blockIdx.y * blockDim.y ;
    unsigned int n = threadIdx.z + blockIdx.z * blockDim.z ;
    if( k < npoint ){
        unsigned int id = k+m*npoint+n*blockDim.y*gridDim.y*npoint;
        out[id] = cuCmul( out[id], make_cuDoubleComplex( 0.0, -1.0));
    }
}

__global__ static void rho_Z2D( int npoint, int overlap, double *in_r, double *in_i, double *out){

    unsigned int id = threadIdx.x + blockIdx.x * blockDim.x ;
    unsigned int k = (id/(npoint-overlap))*npoint + id%(npoint-overlap) + overlap/2;

    out[id] = sqrt( in_r[k]*in_r[k] + in_i[k]*in_i[k]);
  //out[id] =  in_i[k];
}

__global__ static void find_max( int npoint, double *inp, double *out){

    unsigned id = threadIdx.x + blockIdx.x * blockDim.x;
    if( inp[id] > out[id]) out[id] = inp[id];
  //out[id] = inp[id];
}
__global__ static void freqband_cut( int npoint, int nband, int *band, cuDoubleComplex *data, cuDoubleComplex *out){

    unsigned int x = threadIdx.x + blockIdx.x * blockDim.x;
    for(int i=0; i<nband; i++){
        if( x >= band[i] && x < band[i+1]) out[x+npoint*i] = data[x];
    }
}
__global__ static void check_snr( int npoint, int overlap, double *snr, bool *bl){

    unsigned int x = threadIdx.x + blockIdx.x * blockDim.x;
    if( snr[x] > 6.0) bl[ x/(npoint-overlap)] = 1;
}

void matched_filter_X2( straindata *out, cutdataFD idata, chisquartable table, int NGPU, int iGPU){

    waveform wdata = table.wf;
    int nband = table.nband;
    int hbatch = 1; if( wdata.N < hbatch) hbatch = wdata.N; printf("hbatch=%d\n", hbatch);
    int batch = idata.N;
    int npoint_f = idata.npoint; if( npoint_f != wdata.get_npoint()) printf("Error : Tp.npoint != data.npoint\n");
    int npoint_t = npoint_f*2-2;
    int overlap = idata.overlap*idata.strain.srate;

    cuDoubleComplex *ud = (cuDoubleComplex*)malloc_d(sizeof(cuDoubleComplex)*npoint_f*idata.N); 
    cuDoubleComplex *hd = (cuDoubleComplex*)malloc_d(sizeof(cuDoubleComplex)*npoint_f*hbatch); 

    cuDoubleComplex *rfd    = (cuDoubleComplex*)malloc_d(sizeof(cuDoubleComplex)*npoint_f*batch*hbatch); 
    cufftDoubleReal *rtd_r  = (cufftDoubleReal*)malloc_d(sizeof(cufftDoubleReal)*npoint_t*batch*hbatch); 
    cufftDoubleReal *rtd_i  = (cufftDoubleReal*)malloc_d(sizeof(cufftDoubleReal)*npoint_t*batch*hbatch); 
    CHECK( cudaMemset( rfd, 0, sizeof(cuDoubleComplex)*npoint_f*batch*hbatch)); 

    CHECK( cudaMemcpy( ud, idata.data_FD,sizeof(cuDoubleComplex)*idata.N*npoint_f, cudaMemcpyHostToDevice)); 

    double *snr_d = (double*)malloc_d(sizeof(double)*idata.strain.npoint); 
    double *snr_h = (double*)malloc_h(sizeof(double)*idata.strain.npoint); 
    CHECK( cudaMemset( snr_d, 0, sizeof(double)*idata.strain.npoint)); 

    double *chi2_str = (double*)malloc_d(sizeof(double)*idata.strain.npoint); 
    CHECK( cudaMemset( chi2_str, 0, sizeof(double)*idata.strain.npoint)); 
    double *snr_origin = (double*)malloc_d(sizeof(double)*idata.strain.npoint); 
    CHECK( cudaMemset( snr_origin, 0, sizeof(double)*idata.strain.npoint)); 
    double *snr_max = (double*)malloc_d(sizeof(double)*idata.strain.npoint); 
    CHECK( cudaMemset( snr_max, 0, sizeof(double)*idata.strain.npoint)); 

    /* CHISQUARE PART*/
    cuDoubleComplex *rfd_chi    = (cuDoubleComplex*)malloc_d(sizeof(cuDoubleComplex)*npoint_f*nband); 
    cufftDoubleReal *rtd_r_chi  = (cufftDoubleReal*)malloc_d(sizeof(cufftDoubleReal)*npoint_t*nband); 
    cufftDoubleReal *rtd_i_chi  = (cufftDoubleReal*)malloc_d(sizeof(cufftDoubleReal)*npoint_t*nband); 

    int *bandd = (int*)malloc_d(sizeof(int)*(nband+1)*wdata.N);
    CHECK( cudaMemcpy( bandd, table.bandpoint, sizeof(int)*(nband+1)*wdata.N, cudaMemcpyHostToDevice));

    double *chi2 = (double*)malloc_d(sizeof(double)*npoint_t); 

    cufftHandle plan_chi;
    CHECKFFT( cufftPlan1d( &plan_chi, npoint_t, CUFFT_Z2D, nband));
    /* CHISQUARE PART*/

    cufftHandle plan;
    CHECKFFT( cufftPlan1d( &plan, npoint_t, CUFFT_Z2D, batch*hbatch));

    bool *checkd = (bool*)malloc_d(sizeof(bool)*batch*hbatch);
    bool *check = (bool*)malloc_h(sizeof(bool)*batch*hbatch);

    std::complex<double> *hp = wdata.data + npoint_f*hbatch*iGPU;
    for( int i=hbatch*iGPU; i<wdata.N; i+=hbatch*NGPU){
        if( wdata.N-i < hbatch) hbatch = wdata.N-i;

        CHECK( cudaMemcpy( hd, hp, sizeof(cuDoubleComplex)*npoint_f*hbatch, cudaMemcpyHostToDevice));

        dim3 block( nth, 1, 1);
        dim3 grid( (npoint_f+nth-1)/nth, idata.N, hbatch);

        rho_calculate <<< grid, block >>>( npoint_f, ud, hd, rfd);
        CHECKFFT( cufftExecZ2D( plan, (cufftDoubleComplex*)rfd, (cufftDoubleReal*)rtd_r));

        rho_phaser <<< grid, block >>>( npoint_f, rfd);
        CHECKFFT( cufftExecZ2D( plan, (cufftDoubleComplex*)rfd, (cufftDoubleReal*)rtd_i));

        dim3 gridr( (idata.strain.npoint-overlap+nth-1)/nth, 1, 1); // !!!important
        rho_Z2D <<< gridr, block >>>( npoint_t, overlap, rtd_r, rtd_i, &snr_d[overlap/2]);

        dim3 gridm( (idata.strain.npoint+nth-1)/nth, 1, 1);
        find_max <<< gridm, block >>>( idata.strain.npoint, snr_d, snr_origin);

        CHECK( cudaMemset( checkd, 0, sizeof(bool)*batch*hbatch));
        check_snr <<< gridr, block>>>( npoint_t, overlap, &snr_d[overlap/2], checkd);
        CHECK( cudaMemcpy( check, checkd, sizeof(bool)*batch*hbatch, cudaMemcpyDeviceToHost));
        cuDoubleComplex *rfd_point = rfd;
        for(int k=0; k<idata.N; k++){
            if( check[k] || true){
                CHECK( cudaMemset( rfd_chi, 0, sizeof(cuDoubleComplex)*npoint_f*table.nband));

                dim3 gridf( (npoint_f+nth-1)/nth, 1, 1);
                freqband_cut <<< gridf, block>>>( npoint_f, nband, &bandd[i*(nband+1)], rfd_point, rfd_chi);
                CHECKFFT( cufftExecZ2D( plan_chi, (cufftDoubleComplex*)rfd_chi, (cufftDoubleReal*)rtd_i_chi));

                dim3 gridr( (npoint_f+nth-1)/nth, 1, 1);
                rho_phasei <<< gridr, block >>>( npoint_f, rfd_point);
                freqband_cut <<< gridf, block>>>( npoint_f, nband, &bandd[i*(nband+1)], rfd_point, rfd_chi);
                CHECKFFT( cufftExecZ2D( plan_chi, (cufftDoubleComplex*)rfd_chi, (cufftDoubleReal*)rtd_r_chi));

                dim3 gridx( (npoint_t-overlap+nth-1)/nth, 1, 1);
                X2_calculate  <<< gridx, block>>>( npoint_t, overlap, nband
                        , &rtd_r[k*npoint_t], &rtd_i[k*npoint_t] , rtd_r_chi, rtd_i_chi, chi2);
                dim3 gridc( (npoint_t-overlap+nth-1)/nth, 1, 1);
                re_weight_rho <<< gridc, block>>>( npoint_t, overlap, chi2, &snr_d[k*(npoint_t-overlap)]);
                chi2_equ <<< gridc, block>>>( npoint_t, overlap, chi2, &chi2_str[k*(npoint_t-overlap)]);

            }
            rfd_point += npoint_f;
        }
      //dim3 gridm( (idata.strain.npoint+nth-1)/nth, 1, 1);
        find_max <<< gridm, block >>>( idata.strain.npoint, snr_d, snr_max);
      //dim3 gridc( (npoint_t+nth-1)/nth, idata.N);
      //rho_cut <<< gridc, block >>>( npoint_t, idata.overlap*idata.strain.srate, hbatch, rtd_r, rtd_i, snr_max);

        hp += npoint_f*hbatch*NGPU;
    }

    if( out->data == NULL){
        *out = idata.strain;
        out->data = (double*)malloc( 3 * out->npoint * sizeof(double));
        memset( out->data, 0, sizeof(double)*3*out->npoint);
    }
    CHECK( cudaMemcpy( snr_h, snr_max, sizeof(double)*out->npoint, cudaMemcpyDeviceToHost));

    for(int i=0; i<out->npoint; i++) \
        if( snr_h[i] > out->data[i]) out->data[i] = snr_h[i];
    
    CHECK( cudaMemcpy( snr_h, snr_origin, sizeof(double)*out->npoint, cudaMemcpyDeviceToHost));

    for(int i=0; i<out->npoint; i++) \
        if( snr_h[i] > out->data[i+out->npoint]) out->data[i+out->npoint] = snr_h[i];

    CHECK( cudaMemcpy( snr_h, chi2_str, sizeof(double)*out->npoint, cudaMemcpyDeviceToHost));

    for(int i=0; i<out->npoint; i++) \
        if( snr_h[i] > out->data[i+2*out->npoint]) out->data[i+2*out->npoint] = snr_h[i];

    CHECK( cudaFree( ud));
    CHECK( cudaFree( hd));
    CHECK( cudaFree( rfd));
    CHECK( cudaFree( rtd_i));
    CHECK( cudaFree( rtd_r));
    CHECK( cudaFree( rfd_chi));
    CHECK( cudaFree( rtd_i_chi));
    CHECK( cudaFree( rtd_r_chi));
    CHECK( cudaFree( chi2));
    CHECK( cudaFree( bandd));
    CHECK( cudaFree( snr_d));
    CHECK( cudaFree( snr_max));
    CHECK( cudaFree( snr_origin));
    CHECK( cudaFree( chi2_str));
    CHECK( cudaFreeHost( snr_h));
    CHECK( cudaFree( checkd));
    CHECK( cudaFreeHost( check));
    CHECKFFT( cufftDestroy( plan));
    CHECKFFT( cufftDestroy( plan_chi));
}
