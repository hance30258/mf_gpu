/* This file was automatically generated.  Do not edit! */
void waveform_normalize(waveform wfdata,double *PSD);
void waveform_generate(waveform wfdata,Approximant approximant);
void waveform_init_rd(waveform *wfdata,double *max_duration);
void waveform_duration(double m1,double m2,double f_cut);
void lal_fd_waveform(double m1,double m2,double deltaF,Approximant approximant,COMPLEX16FrequencySeries **hptilde,COMPLEX16FrequencySeries **hctilde);
