#include<stdio.h>

#define CHECK(call) \
{ \
const cudaError_t error = call; \
if (error != cudaSuccess) \
{ \
printf("Error: %s:%d, ", __FILE__, __LINE__); \
printf("code:%d, reason: %s\n", error, cudaGetErrorString(error)); \
exit(1); \
} \
}
void *malloc_h( unsigned int k){

    void *data;
  //printf("malloc_h %d MB\n", k/1024/1024);
    CHECK( cudaMallocHost( &data, k));
    return data;
}
void *malloc_d( unsigned int k){

    void *data;
  //printf("malloc_d %d MB\n", k/1024/1024);
    CHECK( cudaMalloc( &data, k));
    return data;
}
void *malloc_u( unsigned int k){

    void *data;
  //printf("malloc_u %d MB\n", k/1024/1024);
    CHECK( cudaMallocManaged( &data, k));
    return data;
}

