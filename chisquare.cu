#include<stdio.h>
#include<math.h>

#include"MFstruct.cpp"

void chisquare_init( chisquartable *table){

    int nband = table->nband;
    int f_min_point = table->wf.f_min*table->wf.duration;
    int f_max_point = table->wf.f_max*table->wf.duration;
    printf("fmin=%g fmax=%g\n",table->wf.f_min,table->wf.f_max);
    if( table->bandpoint==NULL)table->bandpoint = (int*)malloc(sizeof(int)*(nband+1)*table->wf.N);


    std::complex<double> *idata = table->wf.data;
    int *bdp = table->bandpoint;
    for(int i=0; i< table->wf.N; i++){
        printf("m1=%g m2=%g\n",table->wf.m1[i],table->wf.m2[i]);
        double Sump = 1.0/nband;
        double step = 0.;
        *bdp = f_min_point;bdp++;
        for(int j=f_min_point; j<table->wf.get_npoint(); j++){
            step += 4.0* norm(idata[j])/table->PSD[j]/table->wf.duration;
            if( step > Sump ){
                *bdp = j;bdp++;
              //printf("nband = %g Sump = %e step = %e freq = %g \n", \
                        Sump*nband, Sump, step-Sump, j*table->wf.deltaF);
                Sump += 1.0/nband;
                if( Sump > 1.0)break;
            }
        }
        if( Sump <= 1.0) {
          //printf("\nERROR: last freqband unreached\n\n");
            *bdp = f_max_point;bdp++; 
          //printf("nband = %g Sump = %e step = %e freq = %g \n", \
                    Sump*nband, Sump, step-Sump, f_max_point*table->wf.deltaF);
        }
        idata += table->wf.get_npoint();
      //bdp += nband
    }
}
