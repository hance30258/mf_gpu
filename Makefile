CPP = g++ -fopenmp
NVCC = nvcc -Xcompiler -fopenmp -Xcompiler -fPIC -O3

# change CUDA_HOME if needed
CUDA_HOME = /usr/local/cuda-10.0
cuflag = -I$(CUDA_HOME)/include -L$(CUDA_HOME)/lib64 -lcufft -lcudart -lcublas -lcuda
lalflag=$(shell PKG_CONFIG_ALLOW_SYSTEM_CFLAGS=1 pkg-config --libs --cflags-only-I lalsimulation)
fftw3flag=$(shell PKG_CONFIG_ALLOW_SYSTEM_CFLAGS=1 pkg-config --libs --cflags-only-I fftw3) -lfftw3_threads
libframeflag=$(shell PKG_CONFIG_ALLOW_SYSTEM_CFLAGS=1 pkg-config --libs --cflags-only-I libframe)
flag = $(libframeflag) $(fftw3flag) $(lalflag)

obj = calloc.o mf_kernal.o PSD.o chisquare.o X2.o mf_kernal.o program.o
wf_obj_l = lalcode.o 
exe = program

program: $(obj) $(wf_obj_l)
	$(CPP) -o $@ $^ $(flag) $(cuflag)
	
%.o: %.cpp
	$(CPP) -c -o $@ $^ $(flag) $(cuflag)
%.o: %.cu
	$(NVCC) -c -o $@ $^ $(flag) $(cuflag)


all: $(exe)

clean:
	rm -fr *.o $(exe)
